
class Fixnum

  def in_words

    return self.ones if self < 10
    return self.tens if (self > 9) && (self <= 20)
    #debugger
    group_nest = self.group_numbers #[num,group to add at the end e.g million]
    words = Array.new
    #debugger
    group_nest.each { |nest| words << group_conversion(nest) }
    clean_white_space!(words)
    words.reverse.join(" ")

  end

  def group_numbers
    number_group = Hash.new
    number_arr = Array.new

    self_arr = self.to_s.chars
    until self_arr.empty?
      number_arr << self_arr.pop(3).join.to_i
    end # number_arr = [36, 40, 259, 888, 1]

    groups = ["", "thousand", "million", "billion", "trillion"]
    number_arr.each.with_index { |group, i| number_group[groups[i]] = group }
    number_group.to_a.map(&:reverse)

  end

  def group_conversion(nest)

    ending_word = nest[-1]

    numbers = nest[0].to_s.chars.map(&:to_i) #[30] > [3,0]

    ones_group = ones_group_str(numbers)

    tens_group = tens_group_str(numbers)

    hundreds_group = hundreds_group_str(numbers)

    group_string = [hundreds_group, tens_group, ones_group]
    group_string << ending_word if group_string.join != ""
    stitch_groups(group_string)

  end

  def hundreds_group_str(numbers)
    if numbers[-3].nil? || (numbers[-3] == 0)
      ""
    else numbers[-3].hundreds
    end
  end

  def tens_group_str(numbers)
    if numbers[-2].nil? || (numbers[-2] == 0)
      ""
    else format_tens_group(numbers[-2..-1])
    end
  end

  def ones_group_str(numbers)
    if numbers.last == 0
      ""
    elsif numbers[-2].nil? || (numbers[-2] == 0)
      numbers[-1].ones
    else ""
    end
  end

  def format_tens_group(array) #[3,0]

    return "" if array[-2] == 0

    if requires_formatting?(array)
      tens_digit = array[-2].add_a_zero_to
      ones_digit = array[-1]
      tens_word = tens_digit.tens
      ones_word = ones_digit.ones
      tens_word + " " + ones_word
    else
      tens_digit = array.join.to_i
      tens_word = tens_digit.tens
    end

  end

  def requires_formatting?(array) #[3,0]
    #debugger
    tens_digit = array.join.to_i
    value = tens_digit.tens
    return false if value #is not nil i.e we have this value in our hash
    true
  end

  def add_a_zero_to
    new_str = self.to_s + "0"
    new_str.to_i
  end

  def ones
    return "" if self.nil?
    ones = { 0 => "zero", 1 => "one", 2 => "two", 3 => "three",
             4 => "four", 5 => "five", 6 => "six", 7 => "seven",
             8 => "eight", 9 => "nine" }
    ones[self]

  end

  def tens
    return "" if self.nil?
    tens = { 10 => "ten", 11 => "eleven", 12 => "twelve",
             13 => "thirteen", 14 => "fourteen", 15 => "fifteen",
             16 => "sixteen", 17 => "seventeen", 18 => "eighteen",
             19 => "nineteen", 20 => "twenty", 30 => "thirty",
             40 => "forty", 50 => "fifty", 60 => "sixty",
             70 => "seventy", 80 => "eighty", 90 => "ninety" }
    tens[self]

  end

  def hundreds
    self.ones + " " + "hundred"
  end

  def stitch_groups(array)
    array.select { |group| group.length > 1 }.join(" ")
  end

  def clean_white_space!(words)
    words.delete("")
  end

end
